idf_component_register(SRC_DIRS     "./src"
                       INCLUDE_DIRS "./include"
                       REQUIRES     ch405labs_gfx_drivers
                                    gfx
                                    ch405labs_esp_led
                                    ch405labs_esp_controller
                                    ch405labs_gfx_menu
                                    driver)
